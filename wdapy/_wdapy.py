# coding: utf-8

import abc
import base64
import enum
import functools
import io
import json
import typing

import requests
from cached_property import cached_property
from logzero import setup_logger
from PIL import Image

from ._alert import Alert
from ._base import BaseClient
from ._proto import *
from ._types import *
from .exceptions import *
from .usbmux import requests_usbmux, usbmux


class HTTPResponse:
    def __init__(self, resp: requests.Response, err: requests.RequestException):
        self._resp = resp
        self._err = err

    def is_success(self) -> bool:
        return self._err is None and self._resp.status_code == 200

    def json(self) -> dict:
        assert self._resp is not None
        try:
            return self._resp.json()
        except json.JSONDecodeError:
            return RequestError("JSON decode error", self._resp.text)

    def get_error_message(self) -> str:
        if self._resp:
            return self._resp.text
        return str(self._err)

    def raise_if_failed(self):
        if self._err:
            raise RequestError("HTTP request error", self._err)
        if self._resp.status_code != 200:
            raise RequestError(self._resp.status_code, self._resp.text)


class CommonClient(BaseClient):
    def __init__(self, wda_url: str):
        super().__init__(wda_url)
        self.__ui_size = None
        self.__debug = False

    @property
    def debug(self) -> bool:
        return self.__debug

    @debug.setter
    def debug(self, v: bool):
        if v:
            setup_logger(NAME)
        else:
            setup_logger(NAME, level=logging.INFO)

    def app_start(self, bundle_id: str):
        self.session_request(POST, "/wda/apps/launch", {
            "bundleId": bundle_id
        })

    def app_terminate(self, bundle_id: str):
        self.session_request(POST, "/wda/apps/terminate", {
            "bundleId": bundle_id
        })

    def app_state(self, bundle_id: str) -> AppState:
        value = self.session_request(POST, "/wda/apps/state", {
            "bundleId": bundle_id
        })["value"]
        return AppState(value)

    def app_current(self) -> AppInfo:
        self.unlock()
        st = self.status()
        if st.session_id is None:
            self.session()
        data = self.request(GET, "/wda/activeAppInfo")
        value = data['value']
        return AppInfo.value_of(value)

    def app_list(self) -> AppList:
        value = self.session_request(GET, "/wda/apps/list")["value"][0]
        return AppList.value_of(value)

    def deactive(self, duration: float):
        self.session_request(POST, "/wda/deactivateApp", {
            "duration": duration
        })


    @cached_property
    def alert(self) -> Alert:
        return Alert(self)

    def sourcetree(self) -> SourceTree:
        data = self.request(GET, "/source")
        return SourceTree.value_of(data)

    def accessible_source(self) -> AccessibleSource:

    def open_url(self, url: str):
        self.session_request(POST, "/url", {
            "url": url
        })

    def set_clipboard(self, content: str, content_type="plaintext"):
        self.session_request(POST, "/wda/setPasteboard",{
            "content": base64.b64encode(content.encode()).decode(),
            "contentType": content_type
        })

    def appium_settings(self, kwargs: dict = None) -> dict:
        if kwargs is None:
            return self.session_request(GET, "/appium/settings")["value"]
        payload = {"settings": kwargs}
        return self.session_request(POST, "/appium/settings", payload)["value"]

    def is_locked(self) -> bool:
        return self.request(GET, "/wda/locked")["value"]

    def unlock(self):
        self.request(POST, "/wda/unlock")

    def lock(self):
        self.request(POST, "/wda/lock")

    def homescreen(self):
        self.request(POST, "/wda/homescreen")

    def shutdown(self):
        self.request(GET, "/wda/shutdown")

    def get_orientation(self) -> Orientation:
        value = self.session_request(GET, '/orientation')['value']
        return Orientation(value)

    def window_size(self) -> typing.Tuple[int, int]:
        """
        Returns:
            UISize
        """
        # 这里做了一点速度优化，跟进图像大小获取屏幕尺寸
        orientation = self.get_orientation()
        if self.__ui_size is None:
            # 这里认为screenshot返回的屏幕转向时正确的
            pixel_width, pixel_height = self.screenshot().size
            w, h = pixel_width//self.scale, pixel_height//self.scale
            if self.get_orientation() == Orientation.PORTRAIT:
                self.__ui_size = (w, h)
            else:
                self.__ui_size = (h, w)

        if orientation == Orientation.LANDSCAPE:
            return self.__ui_size[::-1]
        else:
            return self.__ui_size

    def tap(self, x: int, y: int):
        self.session_request(POST, "/wda/tap/0", {"x": x, "y": y})


    def swipe(self,
              from_x: int,
              from_y: int,
              to_x: int,
              to_y: int,
              duration: float = 0.2):
        payload = {
            "fromX": from_x,
            "fromY": from_y,
            "toX": to_x,
            "toY": to_y,
            "duration": duration}
        self.session_request(POST, "/wda/dragfromtoforduration", payload)


    def presspress(self, name: Keycode):
        payload = {
            "name": name
        }
        self.session_request(POST, "/wda/pressButton", payload)

    def press_duration(self, name: Keycode, duration: float):
        hid_usages = {
            "home": 0x40,
            "volumeup": 0xE9,
            "volumedown": 0xEA,
            "power": 0x30,
            "snapshot": 0x65,
            "power_plus_home": 0x65
        }
        name = name.lower()
        if name not in hid_usages:
            raise ValueError("Invalid name:", name)
        hid_usages = hid_usages[name]
        payload = {
            "page": 0x0C,
            "usage": hid_usages,
            "duration": duration
        }
        return self.session_request(POST, "/wda/performIoHidEvent", payload)

    @cached_property
    def scale(self) -> int:
        # Response example
        # {"statusBarSize": {'width': 320, 'height': 20}, 'scale': 2}
        value = self.session_request(GET, "/wda/screen")['value']
        return value['scale']

    def status_barsize(self) -> StatusBarSize:
        # Response example
        # {"statusBarSize": {'width': 320, 'height': 20}, 'scale': 2}
        value = self.session_request(GET, "/wda/screen")['value']
        return StatusBarSize.value_of(value['statusBarSize'])

    def screenshot(self) -> Image.Image:
        """ take screenshot """
        value = self.request(GET, "/screenshot")["value"]
        raw_value = base64.b64decode(value)
        buf = io.BytesIO(raw_value)
        im = Image.open(buf)
        return im.convert("RGB")

    def battery_info(self) -> BatteryInfo:
        data = self.session_request(GET, "/wda/batteryInfo")["value"]
        return BatteryInfo.value_of(data)

    @property
    def info(self) -> DeviceInfo:
        return self.device_info()

    def device_info(self) -> DeviceInfo:
        data = self.session_request(GET, "/wda/device/info")["value"]
        return DeviceInfo.value_of(data)


class AppiumClient(CommonClient):
    """
    client for https://github.com/appium/WebDriverAgent
    """

    def __init__(self, wda_url: str = DEFAULT_WDA_URL):
        super().__init__(wda_url)


class AppiumUSBClient(AppiumClient):
    def __init__(self, udid: str = None, port: int = 8100):
        if udid is None:
            _usbmux = usbmux.Usbmux()
            udid = _usbmux.get_single_device_udid()
        super().__init__(requests_usbmux.DEFAULT_SCHEME+udid+f":{port}")


class NanoClient(AppiumClient):
    """
    Repo: https://github.com/nanoscopic/WebDriverAgent

    This repo changes a lot recently and the new version code drop the HTTP API to NNG
    So here use the old commit version
    https://github.com/nanoscopic/WebDriverAgent/tree/d07372d73a4cc4dc0b0d7807271e6d7958e57302
    """

    def tap(self, x: int, y: int):
        """ fast tap """
        self.request(POST, "/wda/tap", {
            "x": x,
            "y": y,
        })

    def swipe(self,
              from_x: int,
              from_y: int,
              to_x: int,
              to_y: int,
              duration: float = .2):
        """ fast swipe """
        self.request(POST, "/wda/swipe", {
            "x1": from_x,
            "y1": from_y,
            "x2": to_x,
            "y2": to_y,
            "delay": duration})


class NanoUSBClient(NanoClient):
    def __init__(self, udid: str = None, port: int = 8100):
        if udid is None:
            _usbmux = usbmux.Usbmux()
            udid = _usbmux.get_single_device_udid()
        super().__init__(requests_usbmux.DEFAULT_SCHEME+udid+f":{port}")
